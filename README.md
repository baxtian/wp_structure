# DESCRIPTION

Class to be inherited to create a WP Structure.

Types:
* checkbox: name, label, description
* date: name, label
* datetime: name, label
* textarea: name, label
* image: name, label, replace
* file: name, label, replace
* video: name, label, replace
* color: name, label
* select: name, label, options
* range: name, label, min, max
* integer|number: name, label

## Mantainers

Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>

## Changelog

## 0.2.34

* Fix bug with date control

## 0.2.32

* Include option to set base structure for permalink

## 0.2.31

* Add options to Color Picker and use Color Palette

## 0.2.30

* Mantain consistency with old meta storages for files.

## 0.2.28

* Use baxtian/wp_columns.

## 0.2.22

* Timber 2.

## 0.2.21

* Solve bugs.

## 0.2.20

* Manage warning about description.

## 0.2.19

* Use direct filename for rendering.

## 0.2.18

* Use loader/paths filter only during rendering.

## 0.2.17

* Declare postmeta if post_type supports custom-fields.

## 0.2.16

* Function to sanitize internally.

## 0.2.15

* Handle can't have "_".

## 0.2.14

* Solving bug "video replace text".

## 0.2.13

* Add type file.

## 0.2.12

* Custom title and replace texts for image and video.

## 0.2.11

* Return emptycolumn when post has image but image doesn't exist.

## 0.2.10

* Add filters to set admin columns contennt and admin columns order

## 0.2.9

* Allow empty data while sanitizing fields in PostMeta

## 0.2.8

* Sanitize textarea

## 0.2.7

* Update i18n

## 0.2.6

* Adding type: video

## 0.2.5

* Solving bg with type range: not saving on change

## 0.2.1

* Adding type: range

## 0.2

* In case of using this class in multiple providers, allow Composer to set which file to use by default.

## 0.1.20

* Update quick edit and bulk edit templates

## 0.1.19

* Allow integer to be edited with quickedit

## 0.1.18

* Adding type: number

## 0.1.17

* Quickedit now works on posts

## 0.1.16

* Allow to use PHP8.0

## 0.1.15

* Bug: not saving image.

## 0.1.14

* Bug fixing.

## 0.1.13

* Bug: Quickedit mo working in posts.

## 0.1.12

* Add sanitization for post meta fields.

## 0.1.11

* Add column viewer for type: url.

## 0.1.10

* Add column viewer for type: select.

## 0.1.9

* Bug: Templates directory not working on some servers.

## 0.1.8

* Use filter to sanitize values in save, quickedit and bulkedit

## 0.1.7

* Allow to save checbox unselected on boxes.

## 0.1.6

* Use ESNext for scripts.

## 0.1.5

* Bug solving

## 0.1.4

* Side color control

## 0.1.3

* UI changes

## 0.1

* First stable release
