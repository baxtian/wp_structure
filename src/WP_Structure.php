<?php

namespace Baxtian;

/**
 * Estructura Película
 */
class WP_Structure
{
	// Datos generales
	protected $slug;
	protected $args;

	/**
	 * Inicializa el componente
	 */
	protected function __construct()
	{
		// Datos OG
		add_filter('get_og', [$this, 'get_og']);

		// Activar el quickedit
		add_action('admin_footer-edit.php', [$this, 'quickedit_script']);

		// Incluir las opciones para determinar cómo será el slug
		add_action('admin_init', [$this, 'add_settings']);
	}

	/**
	 * Registrar la estructura
	 */
	public function init()
	{
		register_post_type($this->slug, $this->args);
	}

	/**
	 * Función para incluir el scrit de quick_edit en caso de estar editando
	 *
	 * @return void
	 */
	public function quickedit_script()
	{
		if (isset($_GET['post_type']) && $this->slug === $_GET['post_type']) {
			$quickedit_vars = apply_filters('quickedit_vars', []);

			//Si hay variables declaradas, ejecutar el script
			if (count($quickedit_vars) > 0) {
				wp_localize_script('postmeta_quick_edit', 'quick_edit', ['vars' => json_encode($quickedit_vars)]);
				wp_print_scripts('postmeta_quick_edit');
			}
		}
	}

	/**
	 * Modificar los datos og para esta estructura
	 *
	 * @param [type] $og
	 * @return void
	 */
	public function get_og($og)
	{
		if (is_singular($this->slug)) {
			//En caso de ser la single de película
		} elseif (is_post_type_archive($this->slug)) {
			//EN caso de ser la archivo de película
			$og['url'] = get_post_type_archive_link($this->slug);
		}

		return $og;
	}

	/**
	 * Incluir la opción en la página de settings de permalink para
	 * determinar cuál será el slug para 'proyecto'
	 *
	 * @return void
	 */
	public function add_settings()
	{
		$option_name = $this->slug . '_base';
		if(isset($this->args['base_setting']) && $this->args['base_setting']) {
			register_setting('permalink', $option_name);

			$label = $this->args['labels']['settings_field'];

			add_settings_field(
				$this->slug . '_base_field',
				$label,
				[$this, 'base_field_callback'],
				'permalink',
				'optional',
				[$option_name]
			);

			if(isset($_POST['permalink_structure']) && isset($_POST[$option_name])) {

				$short_domain = wp_unslash($_POST[$option_name]);
				update_option($option_name, $short_domain);

			}
		} else {
			delete_option($option_name);
		}
	}

	/**
	 * Renderizador de la entrada el setting field del
	 * slug para 'proyecto'
	 *
	 * @param array $args
	 * @return void
	 */
	public function base_field_callback($args)
	{
		$class = 'regular-text code';
		$name  = $args[0];
		$value = get_option($name);
		printf("<input type='text' class='%s' name='%s' id='%s' value='%s' />", $class, $name, $name, $value);
	}
}
