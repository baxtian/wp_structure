<?php

namespace Baxtian;

use Baxtian\WP_Columns_Postmeta;

/**
 * Clase base para los Meta de las estructuras
 */
class WP_PostMeta
{
	// Datos generales
	protected $handle; // Nombre a ser usado como identificador
	protected $prefix; // Prefijo del plugin/tema que usa esta librería
	protected $domain; // Dominio de traducción
	protected $variables       = [];
	protected $file_type_vars = [];
	protected $options         = [];
	protected $post_types      = [];
	protected $label           = '';
	protected $display         = 'side'; //side, block, box
	protected $column_position = -1;
	protected $lang_domain     = '';
	protected $lang_path       = '';

	protected $wp_columns = false;

	/**
	 * Constructor de PostMeta
	 */
	protected function __construct()
	{
		add_action('admin_enqueue_scripts', [$this, 'admin_scripts'], 9);
		add_filter('load_script_textdomain_relative_path', [$this, 'textdomain_relative_path'], 10, 2);
		add_action('admin_enqueue_scripts', [$this, 'admin_enqueue_scripts']);

		add_filter('timber/post/meta', [$this, 'file_post_meta'], 9, 5);

		//Activar sistema de columnas;
		$this->wp_columns = new WP_Columns_Postmeta();
	}

	/**
	 * Internamente se usa el modelo antiguo, así que este sistema pasa la lista de 
	 * variables desde el nuevo formato al antiguo formato
	 *
	 * El antiguo formato era una lísta numerada y el nombre de la variable estaba en 
	 * el campo 'name'. En el nuevo formato el key del item en el arreglo es el name.
	 * @return void
	 */
	private function old_format() {
		$new_variables = $this->variables;
		$this->variables = [];
		foreach($new_variables as $key => $item) {
			// Si no hay 'name' entonces aplicar el key
			if(!isset($item['name'])) {
				$item['name'] = $key;
			}
			$this->variables[] = $item;
		}
	}


	/**
	 * Generar los datos que deben ser calculados, declarar la funcionalidad para el
	 * Rest API, declarar el uso de edición en bloque y edición rápida, y declarar las
	 * columnas para cada estructura con la que se vincule este PostMeta.
	 */
	protected function init()
	{
		// El handle no debe tener "_"
		$this->handle = str_replace('_', '-', $this->handle);

		// Inicializar traducción
		$this->lang_domain = 'wp_structure';
		$locale            = apply_filters('plugin_locale', determine_locale(), $this->lang_domain);

		$wp_component    = basename(WP_CONTENT_URL);
		$_path           = explode($wp_component, __DIR__);
		$this->lang_path = dirname(WP_CONTENT_DIR . $_path[1]) . '/languages/';
		$mofile          = $this->lang_path . $this->lang_domain . '-' . $locale . '.mo';

		load_textdomain($this->lang_domain, $mofile);

		// Pasa la lista de variables del nuevo formato al antiguo
		$this->old_format();

		// Crear el arreglo con las variables que serán editadas por quickedit
		foreach ($this->variables as $key => $item) {
			// Guardar las opciones si es un select
			if ($item['type'] == 'select') {
				$this->options[$item['name']] = $item['options'];
			}

			// Guardar variables de tipo archivo
			if(in_array($item['type'], ['image', 'file', 'video'])) {
				$this->file_type_vars[] = $item['name'];
			}

			// Filtros para sanear
			switch ($item['type']) {
				case 'url':
					add_filter(
						"sanitize_post_meta_{$item['name']}",
						function ($meta_value, $meta_key, $object_type) {
							return esc_url($meta_value);
						},
						10,
						3
					);

					break;
				case 'text':
					add_filter(
						"sanitize_post_meta_{$item['name']}",
						function ($meta_value, $meta_key, $object_type) {
							return sanitize_text_field($meta_value);
						},
						10,
						3
					);

					break;
				case 'textarea':
					add_filter(
						"sanitize_post_meta_{$item['name']}",
						function ($meta_value, $meta_key, $object_type) {
							return sanitize_textarea_field($meta_value);
						},
						10,
						3
					);

					break;
				default:
					break;
			}

			// Modificar postmeta para manejo de campos propios
			// y agregar los postmeta
			foreach ($this->post_types as $post_type) {
				add_post_type_support($post_type, 'custom-fields');
				register_post_meta($post_type, $item['name'], [
					'show_in_rest' => true,
					'single'       => true,
					'type'         => 'string',
				]);
			}

			$this->wp_columns->init(
				$this->post_types,
				$this->handle,
				$this->variables,
				$this->column_position
			);
		}

		// Inicializar en el rest api las variables que son quick edit
		add_action('rest_api_init', [$this, 'api']);

		// Vincular el almacenamiento de la estructura con estas variables
		foreach ($this->post_types as $post_type) {
			// Guardar
			add_action('save_post_' . $post_type, [$this, 'save']);
		}
	}

	/**
	 * Función para declarar los scripts requeridos para el sidebar y gutenberg.
	 * @param  string $hook Gancho
	 */
	public function admin_scripts($hook)
	{
		// Directorio de assets
		$wp_component = basename(WP_CONTENT_URL);
		$path         = str_replace(DIRECTORY_SEPARATOR, '/', explode($wp_component, __DIR__));
		$url          = dirname(content_url($path[1])) . '/' ;

		//Solo activar si estamos en un post_type para esta caja
		$screen = get_current_screen();
		if (!in_array($screen->post_type, $this->post_types)) {
			return;
		}

		//Registrar postmeta-side
		$asset_file   = include(dirname(__DIR__) . "/build/postmeta-side.asset.php");
		wp_register_script('postmeta_side', $url . 'build/postmeta-side.js', $asset_file['dependencies'], $asset_file['version']);
		wp_set_script_translations('postmeta_side', $this->lang_domain, $this->lang_path);

		//Registrar postmeta-block
		$asset_file   = include(dirname(__DIR__) . "/build/postmeta-block.asset.php");
		wp_register_script('postmeta_block', $url . 'build/postmeta-block.js', $asset_file['dependencies'], $asset_file['version']);
		wp_set_script_translations('postmeta_block', $this->lang_domain, $this->lang_path);
	}

	/**
	 * Corrige la URL relatica del script para ser empleado con el traductor de idioma.
	 * @param  string $relative URL relativa al script con los textos a ser traducidos
	 * @param  string $src      URL completa del script
	 * @return string           URL relativa corregida
	 */
	public function textdomain_relative_path($relative, $src)
	{
		// Hacerle creer a WP que el relativo empieza en la raiz de esta
		// librería y no en el vendor de quien llama a esta librería.
		$relative = str_replace('vendor/baxtian/wp_structure/build/', 'build/', $relative);

		return $relative;
	}

	/**
	 * Generador del arreglo a ser enviado con register_rest_field
	 * @param  string $slug        Taxonomía
	 * @param  string $description Nombre del campo
	 * @return array               El arreglo requerido para ser llamado por register_rest_field
	 */
	private function api_args($slug, $description)
	{
		return [
			'get_callback' => function ($object, $field_name, $request) {
				return get_post_meta($object['id'], $field_name, true);
			},
			'update_callback' => function ($value, $object, $field_name) {
				return update_post_meta($object->ID, $field_name, wp_slash($value));
			},
			'schema' => [$slug => $description],
		];
	}

	/**
	 * Registrar el meta term en WPJson.
	 * ToDo: ¿Usar si la estructura ya exisitía y no se ha declarado el schema para estos post meta?
	 */
	public function api()
	{
		foreach ($this->post_types as $post_type) {
			foreach ($this->variables as $item) {
				register_rest_field(
					$post_type,
					$item['name'],
					$this->api_args($item['name'], $item['label'])
				);
			}
		}
	}

	/**
	 * Sanitizar el texto según el tipo
	 *
	 * @param string $val
	 * @param string $key
	 * @param string $handle
	 * @param integer $post_id
	 * @param string $type
	 * @return string
	 */
	private function sanitize($val, $key, $handle, $post_id, $type)
	{
		if ($type == 'textarea') {
			return apply_filters('postmeta_value', sanitize_textarea_field($val), $key, $handle, $post_id);
		}

		return apply_filters('postmeta_value', sanitize_text_field($val), $key, $handle, $post_id);
	}

	/**
	 * Filtro para obtener solo el id si es una variable 
	 * de tipo un archivo: file, image, video.
	 * Esto se hace para mantener consistencia con anteriores 
	 * desarrollos merak en los que se guardaba el id y la url. 
	 * Actualmente solo se guarda el id.
	 *
	 * @param string $object_meta
	 * @param integer $post_id
	 * @param string $field_name
	 * @param object $obj
	 * @param array $args
	 * @return void
	 */
	public function file_post_meta($object_meta, $post_id, $field_name, $obj, $args) {
		// Si el field_name está en la lista de tipo archivos, es un arreglo
		// y el arreglo incluye el id, retornar el id.
		if(in_array($field_name, $this->file_type_vars) && is_array($object_meta) && isset($object_meta['id'])) {
			return $object_meta['id'];
		}

		return $object_meta;
	}

	/**
	 * Guardar los datos de este PostMeta
	 * @param  int $post_id ID de la estructura que se está almacenando
	 */
	public function save($post_id)
	{
		global $pla_save_flag, $post;

		// Verificar que no sea una rutina de autoguardado.
		// Si es así, el formulario no ha sido enviado y por lo tanto cancelamos
		// el proceso.
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return;
		}

		// Revisar permisos del usuario
		if (!current_user_can('edit_post', $post_id)) {
			return;
		}

		// Almacenar variables

		// Si estamos editando desde la vista de edición
		// Como estamos usando javascript / rest, esta parte ya no se usa, pero se
		// deja acá para referencia
		if (isset($_POST['action']) && $_POST['action'] == 'editpost') {
			// Solo guardaremos cosas por POST si este es un metabox
			if ($this->display == 'box') {
				foreach ($this->variables as $item) {
					$key   = $item['name'];
					$val   = (isset($_POST[$key])) ? $_POST[$key] : null;
					$value = $this->sanitize($val, $key, $this->handle, $post_id, $item['type']);
					update_post_meta($post_id, $key, $value);
				}
			}
		}
	}

	/**
	 * Declarar scripts requeridos para un PostMeta tipo block
	 * @param  string $hook Gancho
	 */
	public function admin_enqueue_scripts($hook)
	{
		// Solo activar si estamos en un post_type para esta caja
		$screen = get_current_screen();
		if (!in_array($screen->post_type, $this->post_types)) {
			return;
		}

		if ($this->display == 'side') {
			$this->script_side();
		} elseif ($this->display == 'block') {
			$this->script_block();
		}
	}

	/**
	 * Registro de script para barra lateral
	 *
	 * @return void
	 */
	private function script_side(): void
	{
		// Registrar script box
		wp_enqueue_script('postmeta_side');

		// Argumentos
		$args = [
			'name'        => $this->handle . '-postmeta',
			'title'       => $this->label,
			'description' => ($this->variables[0]['description']) ?? '',
			'keywords'    => [
				$this->variables[0]['label'],
			],
			'vars'   => $this->variables,
			'prefix' => $this->prefix,
			'domain' => $this->domain,
		];

		// Las variables a ser enviadas para este bloque
		wp_add_inline_script('postmeta_side', "
			try{
				postmetaSide('" . json_encode($args) . "');
			}catch(e){}");
	}

	/**
	 * Registro de script para bloque
	 *
	 * @return void
	 */
	private function script_block(): void
	{
		// Registrar script box
		wp_enqueue_script('postmeta_block');

		// Argumentos
		$args = [
			'name'     => $this->handle . '-postmeta',
			'title'    => $this->label,
			'keywords' => [
				$this->variables[0]['label'],
			],
			'vars'   => $this->variables,
			'prefix' => $this->prefix,
			'domain' => $this->domain,
		];

		// Las variables a ser enviadas para este bloque
		wp_add_inline_script('postmeta_block', "
			try{
				postmetaBlock('" . json_encode($args) . "');
			}catch(e){}");
	}
}
