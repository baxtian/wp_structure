��    
      l      �       �      �      �      
          $     2     @  	   I  	   S  T  ]     �     �     �     �               *     :     I               
   	                           Remove file Remove image Remove video Replace file Replace image Replace video Set file Set image Set video Project-Id-Version: wp_structure
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-03-09 11:20-0500
Last-Translator: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language-Team: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;esc_attr_e;esc_attr__
X-Poedit-Basepath: ..
X-Generator: Poedit 3.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: src
X-Poedit-SearchPath-1: build
 Eliminar el archivo Eliminar la imagen Eliminar el video Reemplazar el archivo Reemplazar la imagen Reemplazar el video Asignar archivo Asignar imagen Establecer el video 