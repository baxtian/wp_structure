const { __ } = wp.i18n;
import {
	TextControl,
	CheckboxControl,
	SelectControl,
} from '@wordpress/components';

import { registerBlockType } from '@wordpress/blocks';

global.postmetaBlock = function (json) {
	var data = JSON.parse(json);

	var el = window.wp.element.createElement;
	const { withDispatch, withSelect } = wp.data;
	const { compose } = wp.compose;

	var MetaTextControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		return <TextControl
			label={props.title}
			value={props.metaValue}
			onChange={function (content) {
				props.setMetaValue(content);
			}} />;
	});

	var MetaTextareaControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		return <TextareaControl
			label={props.title}
			value={props.metaValue}
			onChange={function (content) {
				props.setMetaValue(content);
			}} />;
	});

	var MetaCheckboxControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (isChecked) {
				dispatch('core/editor').editPost({
					[props.metaKey]: isChecked
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		const title = (props.title) ? <div style={{ marginBottom: '4px' }}>{props.title}</div> : null;
		return <div className={'components-base-control'}>
			{title}
			<CheckboxControl
				label={props.description}
				checked={props.metaValue}
				onChange={function (isChecked) {
					props.setMetaValue(isChecked);
				}} />
		</div>;
	});

	var MetaSelectControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		var options = props.options;

		return <SelectControl
			style={{ display: 'block' }}
			label={props.title}
			value={props.metaValue}
			options={options}
			onChange={function (content) {
				props.setMetaValue(content);
			}} />;
	});

	registerBlockType(data.prefix + '/' + data.name, {
		title: data.title,
		icon: 'media-code',
		category: 'layout',
		keywords: data.keywords,
		supports: {
			inserter: false, //Este plugin está pensado para ser usado en un templade de una estructura
			reusable: false,
			html: false
		},

		edit: function (props) {
			var className = props.className;
			var setAttributes = props.setAttributes;

			var controls = data.vars.map(function (item) {
				if (item.type == 'checkbox') {
					return <MetaCheckboxControl
						metaKey={item.name}
						title={item.label}
						description={item.description}
					/>;
				} else if (item.type == 'select') {
					return <MetaSelectControl
						metaKey={item.name}
						title={item.label}
						options={item.options}
					/>;
				} else {
					return <MetaTextControl
						metaKey={item.name}
						title={item.label}
					/>;
				}
			});

			return <div className={className}>{controls}</div>;
		},

		// No information saved to the block
		// Data is saved to post meta via attributes
		save: function () {
			return null;
		},
	});
}
