const { __ } = wp.i18n;
import {
	TextareaControl,
	TextControl,
	DatePicker,
	DateTimePicker,
	Button,
	CheckboxControl,
	SelectControl,
	RangeControl,
	ColorPicker,
	ColorPalette,
	Icon
} from '@wordpress/components';
import { Card, CardMedia } from '@wordpress/components';
import { MediaUpload } from '@wordpress/block-editor';

import { registerPlugin } from '@wordpress/plugins';
import { PluginDocumentSettingPanel } from '@wordpress/edit-post';

import { useState, useEffect } from '@wordpress/element';

import apiFetch from '@wordpress/api-fetch';

global.postmetaSide = function (json) {
	var data = JSON.parse(json);

	const { withDispatch, withSelect } = wp.data;
	const { compose } = wp.compose;

	var generic_style = { marginBottom: '1.5em' };

	var MetaTextControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		return <TextControl
			style={generic_style}
			label={props.title}
			value={props.metaValue}
			onChange={function (content) {
				props.setMetaValue(content);
			}}
		/>;
	});

	var MetaIntegerControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		return <TextControl
			type="number"
			style={generic_style}
			label={props.title}
			value={props.metaValue}
			onChange={function (content) {
				props.setMetaValue(content);
			}}
		/>;
	});

	var MetaRangeControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		let min = (props.min == null) ? 1 : props.min;
		let max = (props.max == null) ? 5 : props.max;
		let value = props.metaValue;
		if (value > max) value = max;
		if (value < min) value = min;
		value = parseInt(value);
		return <RangeControl
			style={generic_style}
			label={props.title}
			value={value}
			min={min}
			max={max}
			onChange={function (content) {
				props.setMetaValue(content);
			}}
		/>;
	});

	var MetaTextareaControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		return <TextareaControl
			style={generic_style}
			label={props.title}
			value={props.metaValue}
			rows={15}
			onChange={function (content) {
				props.setMetaValue(content);
			}}
		></TextareaControl>;
	});

	var MetaDateControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		const date = (props.metaValue == '') ? new Date() : new Date(props.metaValue);
		const date_string = date.toISOString().slice(0, 10);
		return <>
			<h2>{props.title}</h2>
			<DatePicker
				style={generic_style}
				label={props.title}
				currentDate={date_string + ' 00:00'}
				onChange={function (content) {
					var newDateTime = moment(content).format('YYYY-MM-DD');
					props.setMetaValue(newDateTime);
				}}
			/>
		</>;
	});

	var MetaDateTimeControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		return <>
			<h2>{props.title}</h2>
			<DateTimePicker
				style={generic_style}
				label={props.title}
				currentDate={props.metaValue}
				is12Hour={true}
				onChange={function (content) {
					var newDateTime = moment(content).format('YYYY-MM-DD HH:mm');
					props.setMetaValue(newDateTime);
				}}
			/>
		</>;
	});

	var MetaImageControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		const [fileData, setFileData] = useState(false);
		const [fileURL, setFileURL] = useState('');

		useEffect(() => {
			const fetchTitle = async () => {
				const id = (props.metaValue.id !== undefined) ? props.metaValue.id : props.metaValue;
				if (props.metaValue.id !== undefined) {
					props.setMetaValue(id);
				}
				if (id !== undefined && id != '') {
					apiFetch({ path: '/wp/v2/media/' + id }).then((media) => {
						setFileData(media);
						setFileURL(media.source_url);
					}).catch((error) => { });
				}
			};
			fetchTitle();
		}, []);

		var button_style = { display: 'block', marginTop: '1em' };
		var title_txt = (props.title) ?? __('Set image', 'wp_structure');
		var replace_txt = (props.replace) ?? __('Replace image', 'wp_structure');
		var link_to_remove = (props.metaValue !== undefined && props.metaValue != '') ?
			<Button
				variant="link"
				// icon="trash"
				// iconPosition="right"
				isDestructive={true}
				style={button_style}
				onClick={function () {
					setFileData(false);
					setFileURL('');
					props.setMetaValue('');
				}}>{__('Remove image', 'wp_structure')}</Button> : '';
		return <div style={generic_style}>
			<MediaUpload
				allowedTypes={['image']}
				style={generic_style}
				value={fileData.id}
				type={'image'}
				onSelect={function (newMedia) {
					props.setMetaValue(newMedia.id);
					setFileData(newMedia);
					setFileURL(newMedia.url);
				}}
				render={function (obj) {
					if (fileData === false || typeof fileData === 'undefined') {
						return <Button
							onClick={obj.open}
							className={'editor-post-featured-image__toggle'}
						>{title_txt}</Button>
					} else {
						return <>
							<div
								style={{ cursor: 'pointer' }}
								onClick={obj.open}>
								<img
									src={fileURL}
									onClick={obj.open}
								/></div>
							<Button
								isLarge={true}
								isDefault={true}
								onClick={obj.open}
								style={button_style}
							>{replace_txt}</Button>
						</>;
					}
				}}
			/>
			{link_to_remove}
		</div>;
	});

	var MetaFileControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		const [fileData, setFileData] = useState(false);
		const [fileTitle, setFileTitle] = useState('');

		useEffect(() => {
			const fetchTitle = async () => {
				const id = (props.metaValue.id !== undefined) ? props.metaValue.id : props.metaValue;
				if (props.metaValue.id !== undefined) {
					props.setMetaValue(id);
				}
				if (id !== undefined && id != '') {
					apiFetch({ path: '/wp/v2/media/' + id }).then((media) => {
						setFileData(media);
						setFileTitle(media.title.rendered);
					}).catch((error) => { });
				}
			};
			fetchTitle();
		}, []);

		var button_style = { display: 'block', marginTop: '1em' };
		var title_txt = (props.title) ?? __('Set file', 'wp_structure');
		var replace_txt = (props.replace) ?? __('Replace file', 'wp_structure');
		var link_to_remove = (props.metaValue !== undefined && props.metaValue != '') ?
			<Button
				variant="link"
				// icon="trash"
				// iconPosition="right"
				isDestructive={true}
				style={button_style}
				onClick={function () {
					setFileData(false);
					setFileTitle('');
					props.setMetaValue('');
				}}>{__('Remove file', 'wp_structure')} </Button> : '';
		return <div style={generic_style}>
			<MediaUpload
				allowedTypes={['text', 'application']}
				style={generic_style}
				value={fileData.id}
				onSelect={function (newMedia) {
					props.setMetaValue(newMedia.id);
					setFileData(newMedia);
					setFileTitle(newMedia.title);
				}}
				render={function (obj) {
					if (fileData === false || typeof fileData === 'undefined') {
						return <Button
							onClick={obj.open}
							className={'editor-post-featured-image__toggle'}
						>{title_txt}</Button>
					} else {
						return <>
							<Button
								onClick={obj.open}
								className={'editor-post-featured-image__toggle'}
								icon="media-document"
							>{fileTitle}</Button>
							<Button
								isLarge={true}
								isDefault={true}
								onClick={obj.open}
								style={button_style}
							>{replace_txt}</Button>
						</>;
					}
				}}
			/>
			{link_to_remove}
		</div >;
	});

	var MetaVideoControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		const [fileData, setFileData] = useState(false);
		const [fileURL, setFileURL] = useState('');

		useEffect(() => {
			const fetchTitle = async () => {
				const id = (props.metaValue.id !== undefined) ? props.metaValue.id : props.metaValue;
				if (props.metaValue.id !== undefined) {
					props.setMetaValue(id);
				}
				if (id !== undefined && id != '') {
					apiFetch({ path: '/wp/v2/media/' + id }).then((media) => {
						setFileData(media);
						setFileURL(media.source_url);
					}).catch((error) => { });
				}
			};
			fetchTitle();
		}, []);

		var button_style = { display: 'block', marginTop: '1em' };
		var title_txt = (props.title) ?? __('Set video', 'wp_structure');
		var replace_txt = (props.replace) ?? __('Replace video', 'wp_structure');
		var link_to_remove = (props.metaValue !== undefined && props.metaValue != '') ?
			<Button
				variant="link"
				// icon="trash"
				// iconPosition="right"
				isDestructive={true}
				style={button_style}
				onClick={function () {
					setFileData(false);
					setFileURL('');
					props.setMetaValue('');
				}}>{__('Remove video', 'wp_structure')} </Button> : '';
		return <div style={generic_style}>
			<MediaUpload
				allowedTypes={['video']}
				style={generic_style}
				value={fileData.id}
				type={'video'}
				onSelect={function (newMedia) {
					props.setMetaValue(newMedia.id);
					setFileData(newMedia);
					setFileURL(newMedia.url);
				}}
				render={function (obj) {
					if (fileData === false || typeof fileData === 'undefined') {
						return <Button
							onClick={obj.open}
							className={'editor-post-featured-image__toggle'}
						>{title_txt}</Button>
					} else {
						return <>
							<Card>
								<CardMedia>
									<video
										src={fileURL}
										onClick={obj.open}
									/>
								</CardMedia>
							</Card>
							<Button
								isLarge={true}
								isDefault={true}
								onClick={obj.open}
								style={button_style}
							>{replace_txt}</Button>
						</>;
					}
				}}
			/>
			{link_to_remove}
		</div>;
	});

	var MetaCheckboxControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (isChecked) {
				dispatch('core/editor').editPost({
					[props.metaKey]: isChecked
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		const title = (props.title) ? <div style={{ marginBottom: "4px" }}>{props.title}</div> : null;
		return <div className={'components-base-control'}>
			{title}
			<CheckboxControl
				style={generic_style}
				checked={props.metaValue}
				label={props.description}
				onChange={function (isChecked) {
					props.setMetaValue(isChecked);
				}}
			/>
		</div>;
	});

	var MetaSelectControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		var options = props.options;

		return <SelectControl
			// style={generic_style}
			label={props.title}
			value={props.metaValue}
			options={options}
			onChange={function (content) {
				props.setMetaValue(content);
			}} />;
	});

	var MetaColorControl = compose(withDispatch(function (dispatch, props) {
		return {
			setMetaValue: function (metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function (select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function (props) {
		const title = (props.title) ? <div style={{ marginBottom: "4px" }}>{props.title}</div> : null;

		const picker = (!props.options) ? <ColorPicker
			style={generic_style}
			label={props.title}
			color={props.metaValue}
			onChange={function (content) {
				props.setMetaValue(content);
			}}
			disableAlpha={true}
		/> : <ColorPalette
			style={generic_style}
			label={props.title}
			value={props.metaValue}
			colors={props.options}
			onChange={function (content) {
				props.setMetaValue(content);
			}}
			disableAlpha={true}
		/>

		return <div className={'components-base-control'}>
			{title}
			{picker}
		</div>;
	});

	registerPlugin(data.name, {
		icon: "none",
		render: function (props) {
			var controls = data.vars.map(function (item) {
				if (item.type == 'checkbox') {
					return <MetaCheckboxControl
						metaKey={item.name}
						title={item.label}
						description={item.description}
					/>;
				} else if (item.type == 'date') {
					return <MetaDateControl
						metaKey={item.name}
						title={item.label}
					/>
				} else if (item.type == 'datetime') {
					return <MetaDateTimeControl
						metaKey={item.name}
						title={item.label}
					/>;
				} else if (item.type == 'textarea') {
					return <MetaTextareaControl
						metaKey={item.name}
						title={item.label}
					/>;
				} else if (item.type == 'file') {
					return <MetaFileControl
						metaKey={item.name}
						title={item.label}
						replace={item.replace}
					/>;
				} else if (item.type == 'image') {
					return <MetaImageControl
						metaKey={item.name}
						title={item.label}
						replace={item.replace}
					/>;
				} else if (item.type == 'video') {
					return <MetaVideoControl
						metaKey={item.name}
						title={item.label}
						replace={item.replace}
					/>;
				} else if (item.type == 'color') {
					return <MetaColorControl
						metaKey={item.name}
						title={item.label}
						options={item.options ? item.options : false}
					/>;
				} else if (item.type == 'select') {
					return <MetaSelectControl
						metaKey={item.name}
						title={item.label}
						options={item.options}
					/>;
				} else if (item.type == 'range') {
					return <MetaRangeControl
						metaKey={item.name}
						title={item.label}
						min={item.min}
						max={item.max}
					/>;
				} else if (item.type == 'integer' || item.type == 'number') {
					return <MetaIntegerControl
						metaKey={item.name}
						title={item.label}
					/>;
				} else {
					return <MetaTextControl
						metaKey={item.name}
						title={item.label}
					/>;
				}
			});

			return <PluginDocumentSettingPanel
				name={"custom-panel"}
				title={data.title}
				className={"custom-panel"}
			>{controls}</PluginDocumentSettingPanel>;
		},
	});
}
